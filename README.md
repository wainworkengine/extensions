# Module of WainWorkEngine

No additional dependencies

## Getting started

Add this to your Packages\manifest.json
```
"com.wainwork.engine.extensions": "https://gitlab.com/wainworkengine/extensions.git#1.0.0",
```


## How to use

Has a lot of extensions for easy use.

## BaseUIExtensions

```
// Reset position, scale an rotation to default
RectTransform ResetLocal(this RectTransform rectTransform)

// Add callback to button click event. Remove it first to avoid duplication.
void AddOnButtonClickSafe(this Button button, UnityAction callback)

// Add callback to default unity slider chage value event. Remove it first to avoid duplication.
void AddOnSliderChangedSafe(this Slider slider, UnityAction<float> callback)

// Set string to default unity text component. Chack for null to make it safety.
void SetTextSafe(this Text cmp, string text)

// Set anchors and positions to fill the entire space of the parent object.
void StretchToWhole(this RectTransform rectTransform)
```

## GenericExtensions

```
// Find component in GameObject or in all children.
T FindComponentOrInChild<T>(this GameObject t) where T : Component

// Find component in Transform or in all children.
T FindComponentOrInChild<T>(this Transform t) where T : Component

// Add Component to game object. If component already exist on game object just return it.
T AddComponentOnce<T>(this GameObject go) where T : Component

// If cmp not null invoke SUCCESS, else invoke UNSUCCESSFULLY. Use to continue w/o null checking.
T DoWithSafe<T>(this T cmp, Action<T> action, Action fallbackAction = null)

// If cmp not null invoke SUCCESS, else invoke UNSUCCESSFULLY. Use to continue w/o cast checking.
T As<T>(this object cmp, Action<T> success = null, Action unsuccessfully = null)
```

## BaseExtensions

```
// Check object for class or struct and equals for null or default.
public static bool IsNullOrDefault<T>(this T obj)

//Set Layer to target game object and all children
public static void SetLayerRecursive(this Transform go, int layer)

// Reset transform to default.
public static Transform ResetLocal(this Transform transform, bool local = true, bool position = true, bool rotation = true, bool scale = true)

// Return new vector3 with 0 value of ignored axis.
public static Vector3 IgnoreAxis(this Vector3 v, Vector3Axises ignoredAxis)

// Find child by name. Even if he is turned off.
public static Transform FindInactive(this Transform t, string name)

// Find child by name recursive. Even if he is turned off.
public static Transform FindChildRecursive(this Transform t, string name)

public static DateTime ConvertToData(this string value)

// Copy main rect transform features FROM parameters.
public static void CopyFrom(this RectTransform rt1, RectTransform rt2)

// Set enable component SAFE!
public static void SetCmpActiveSafe(this Behaviour cmp, bool isEnable)
```

## EnumeratorsExtensions

```
// Get string which contain every element string formatted.
string GetAsString<T>(this IEnumerable<T> enumerator, string format = "{0}")

// Get string which contain every element string formatted.
string GetAsString(this Array array, string format = "{0}")

// Get random element from collection.
T GetRandom<T>(this IEnumerable<T> array)

// Return NEW collection w/o duplicated elements.
IEnumerable<T> GetOnlyUniq<T>(this IEnumerable<T> array)

// Return NEW collection with all elements which are not null.
IEnumerable<T> RemoveNull<T>(this IEnumerable<T> enumeration)

// Invoke action for every of element in collection.
void ForAll<T>(this IEnumerable<T> enumeration, Action<T> action)

// Return NEW collection with elements which place between index and length.
IEnumerable<T> SubArray<T>(this T[] data, int index, int length)

// Return NEW collection with elements which place between 0 and slotAmount.
IList<T> GetAsArray<T>(this IEnumerable<T> items, int slotAmount)
```

## StringExtensions

```
// All default unescape symbols
public Dictionary<string, char> BASE_UNESCAPE_SYMBOLS

// Convert string to RichText. if map is null, apply default unescape symbols.
public string ToRichText(this string str, Dictionary<string, char> map = default)
```
